# EDAT
ExploitDevelopmentAssistingTools

EDAT is a bunch of tools like lengther, multiplier that can assist you in exploit development.
Lengther can tell you how big a shellcode is when u input it in ascii format.
Multiplier prints how many times an ascii character to be printed out.
These tools might seem "weak" and simple and they are but trust me you might need it to save time.

<strong>USAGE lengther:</strong>
<pre><code>python lengther.py</code></pre>

<strong>USAGE multiplier:</strong>
<pre><code>python multiplier.py</code></pre>

Author: Thanasis Tserpelis aka trickster0

|| Copyright 2017 (License) ||

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
